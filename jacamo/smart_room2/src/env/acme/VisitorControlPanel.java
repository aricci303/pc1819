package acme;
import javax.swing.*;

import java.awt.Dimension;
import java.awt.event.*;
import cartago.*;
import cartago.tools.*;
import javax.swing.event.*;

public class VisitorControlPanel extends GUIArtifact {

	private String visitorName;
	private VisitorGUIFrame frame;
	
	void init(String visitorName) {
		this.visitorName = visitorName;
		super.init();
	}
	
	public void setup() {
		try {
			int preferredTemp = 20;
			frame = new VisitorGUIFrame(visitorName, preferredTemp, this);
	 		defineObsProperty("preferred_temperature",preferredTemp);
			frame.setVisible(true);		
		} catch (Exception ex){
			ex.printStackTrace();
		}
	}

	@INTERNAL_OPERATION void setPreferredTemperature(int value){
        getObsProperty("preferred_temperature").updateValue(value);
	}
	

	@INTERNAL_OPERATION void notifyEntered(){
		signal("user_entered_the_room");
	}

	@INTERNAL_OPERATION void notifyLeft(){
		signal("user_left_the_room");
	}
	

	@INTERNAL_OPERATION void closed(WindowEvent ev){
		System.exit(0);
	}
		
	class VisitorGUIFrame extends JFrame {		
		
		private JTextField tempValue;
		private JButton enter, leave;
		private JSlider temp;
		private VisitorControlPanel artifact;
		
		public VisitorGUIFrame(String visitorName, int startTemp, VisitorControlPanel artifact){
			setTitle("..:: Visitor "+visitorName+" Control Panel ::..");
			setSize(400,200);
			
			this.artifact = artifact;
			
			JPanel mainPanel = new JPanel();
			mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
			setContentPane(mainPanel);
			
			JPanel temperature = new JPanel();
			temperature.setLayout(new BoxLayout(temperature, BoxLayout.Y_AXIS));

			JPanel temperature1 = new JPanel();
			temperature1.setLayout(new BoxLayout(temperature1, BoxLayout.X_AXIS));
			
			tempValue = new JTextField(5);
			tempValue.setText(""+startTemp);
			tempValue.setSize(100, 30);
			tempValue.setMinimumSize(tempValue.getSize());
			tempValue.setMaximumSize(tempValue.getSize());
			tempValue.setEditable(false);
			
			temperature1.add(new JLabel("Pref. Temperature:"));
			temperature1.add(Box.createRigidArea(new Dimension(0,5)));
			temperature1.add(tempValue);
			
			temp = new JSlider(JSlider.HORIZONTAL, 5, 45, startTemp);
			temp.setSize(300, 60);
			temp.setMinimumSize(temp.getSize());
			temp.setMaximumSize(temp.getSize());
			temp.setMajorTickSpacing(10);
			temp.setMinorTickSpacing(1);
			temp.setPaintTicks(true);
			temp.setPaintLabels(true);

			temp.addChangeListener(ev -> {
				JSlider source = (JSlider) ev.getSource();
		        int value = (int) source.getValue();	
		        tempValue.setText("" + value);
				if (!source.getValueIsAdjusting()) {
					artifact.beginExternalSession();
		            artifact.setPreferredTemperature(value);
		            artifact.endExternalSession(true);
				}				
			});

			temperature.add(temperature1);
			temperature.add(temp);
			
			JPanel buttons = new JPanel();
			buttons.setLayout(new BoxLayout(buttons, BoxLayout.X_AXIS));

			enter = new JButton("Enter Room");
			enter.setSize(100, 30);
			enter.setMinimumSize(enter.getSize());
			enter.setMaximumSize(enter.getSize());
			
			enter.addActionListener(ev -> {
				enter.setEnabled(false);
				leave.setEnabled(true);
				artifact.beginExternalSession();
				artifact.notifyEntered();
				artifact.endExternalSession(true);
			});
			
			leave = new JButton("Leave Room");
			leave.setSize(100, 30);
			leave.setMinimumSize(leave.getSize());
			leave.setMaximumSize(leave.getSize());
			
			leave.addActionListener(ev -> {
				enter.setEnabled(true);
				leave.setEnabled(false);
				artifact.beginExternalSession();
				artifact.notifyLeft();
				artifact.endExternalSession(true);
			});
			
			buttons.add(enter);
			buttons.add(Box.createRigidArea(new Dimension(0,5)));
			buttons.add(leave);

			enter.setEnabled(true);
			temp.setEnabled(true);
			leave.setEnabled(false);

			mainPanel.add(temperature);
			mainPanel.add(buttons);
		}
	}
}
