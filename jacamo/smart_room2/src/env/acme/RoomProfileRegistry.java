package acme;

import cartago.*;
import java.util.*;

public class RoomProfileRegistry extends Artifact {

	private long id;
	private HashMap<Long,UserProfile> profiles;
	
	void init(){
		id = 0;
		profiles = new HashMap<Long,UserProfile>();
		defineObsProperty("target_temperature",18);
	}
	
	@OPERATION void register(String userName, OpFeedbackParam<Long> userId){
		id++;
		userId.set(id);
		defineObsProperty("visitor",id,userName);
		profiles.put(id, new UserProfile());		
		updateTargetTemp();
	}

	@OPERATION void deregister(long id){
		this.removeObsPropertyByTemplate("visitor",id,null);
		profiles.remove(id);
		updateTargetTemp();	
	}

	@OPERATION void storePreferredTemperature(long id, double value){
		UserProfile profile = profiles.get(id);
		if (profile!=null){
			profile.setPreferredTemperature(value);
			updateTargetTemp();		
		}
	}
	
	private void updateTargetTemp(){
			double average = 0;
			int np = 0;
			for (UserProfile p: profiles.values()){
				if (p.getPreferredTemperature()!=null){
					average+=p.getPreferredTemperature(); 
					np++;
				}
			}
			if (np > 0){
				average /= np;
				getObsProperty("target_temperature").updateValue(average);
			}
	}
	
	
	class UserProfile {

		private Double temperature;
		
		public UserProfile(){
			temperature = null;
		}
		
		public void setPreferredTemperature(double value){
			temperature = value;
		}
		
		public Double getPreferredTemperature(){
			return temperature;
		}
	}
}
