/*
 * USER ASSISTANT AGENT 
 *
 *
 */
!boot.

+!boot  
	<- 	.my_name(Name);
		+user_name(Name);
		.concat("visitor_gui_",Name,ArtName);
		makeArtifact(ArtName,"acme.VisitorControlPanel",[Name],Id);
		focus(Id).

+preferred_temperature(T) : user_name(Name) & user_id(Id)
	<-  println("new preferred temperature for visitor ",Name,": ",T);
		storePreferredTemperature(Id,T);
		println("new preferred temperature stored.").
		
+user_entered_the_room 
	<- 	!register.
	
+user_left_the_room
	<- 	!deregister.

@register_plan [atomic]	
+!register : user_name(Name) & preferred_temperature(T)
	<-	register(Name,Id);
		+user_id(Id);
		println("registration succeeded.");
		storePreferredTemperature(Id,T);
		println("preferred temperature stored.").

		
-!register
	<-	println("registration failed.").

@deregister_plan [atomic]	
+!deregister : user_id(Id)
	<-	deregister(Id);
		println("deregistration succeeded.").
		
-!deregister
	<-	println("registration failed.").
		
			

	

	

