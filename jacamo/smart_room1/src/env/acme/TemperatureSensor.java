package acme;
import javax.swing.*;

import java.awt.Dimension;
import java.awt.event.*;
import cartago.*;
import cartago.tools.*;

public class TemperatureSensor extends GUIArtifact {

	private TemperatureSensorGUIFrame frame;
	
	public void setup() {
		try {
			defineObsProperty("current_temperature",18);		

			frame = new TemperatureSensorGUIFrame(this);
			// linkActionEventToOp(frame.tempButton,"notifyNewTemperature");
			// linkKeyStrokeToOp(frame.target,"ENTER","notifyNewTemperature");
			// linkWindowClosingEventToOp(frame, "closed");
			frame.setVisible(true);		
		} catch (Exception ex){
			ex.printStackTrace();
		}
	}

	@OPERATION void updateTemperature(double delta){
		ObsProperty prop = getObsProperty("current_temperature");
		double newValue = prop.doubleValue()+delta; 
		prop.updateValue(newValue);
		frame.setTempValue(newValue);
	}
	
	@INTERNAL_OPERATION void notifyNewTemperature(double newValue){
		ObsProperty prop = getObsProperty("current_temperature");
		prop.updateValue(newValue);
		frame.setTempValue(newValue);
	}

	@INTERNAL_OPERATION void closed(WindowEvent ev){
		System.exit(0);
	}
		
	class TemperatureSensorGUIFrame extends JFrame {		
		
		private JButton tempButton;
		private JTextField target;
		private JTextField tempValue;
		private TemperatureSensor artifact;
		
		public TemperatureSensorGUIFrame(TemperatureSensor artifact){
			this.artifact = artifact;
			setTitle("..:: Room Temperature Sensor ::..");
			setSize(400,100);
			
			JPanel mainPanel = new JPanel();
			mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
			setContentPane(mainPanel);

			JPanel currentTempPanel = new JPanel();
			currentTempPanel.setLayout(new BoxLayout(currentTempPanel, BoxLayout.X_AXIS));
			
			tempValue = new JTextField(5);
			tempValue.setText("0");
			tempValue.setSize(100, 30);
			tempValue.setMinimumSize(tempValue.getSize());
			tempValue.setMaximumSize(tempValue.getSize());
			tempValue.setEditable(false);

			currentTempPanel.add(new JLabel("Current temperature: "));
			currentTempPanel.add(tempValue);

			//
			
			JPanel setTempPanel = new JPanel();
			setTempPanel.setLayout(new BoxLayout(setTempPanel, BoxLayout.X_AXIS));

			tempButton = new JButton("set");
			tempButton.setSize(80,50);
			
			tempButton.addActionListener(ev -> {
				artifact.beginExternalSession();
				try {
					double newValue = Double.parseDouble(target.getText());
					artifact.notifyNewTemperature(newValue);
					artifact.endExternalSession(true);
				} catch (Exception ex){
					ex.printStackTrace();
					artifact.endExternalSession(false);
				}				
			});
			target = new JTextField(5);
			target.setText("0");
			target.setSize(100, 30);
			target.setMinimumSize(target.getSize());
			target.setMaximumSize(target.getSize());
			target.setEditable(true);
			
			setTempPanel.add(new JLabel("Set temperature: "));
			setTempPanel.add(target);
			setTempPanel.add(Box.createRigidArea(new Dimension(0,5)));
			setTempPanel.add(tempButton);
			
			mainPanel.add(currentTempPanel);
			setTempPanel.add(Box.createRigidArea(new Dimension(10,0)));
			mainPanel.add(setTempPanel);
		}
		
		public void setTempValue(double v){
			SwingUtilities.invokeLater(() -> {
				tempValue.setText(""+v);
			});
		}
	}
}
