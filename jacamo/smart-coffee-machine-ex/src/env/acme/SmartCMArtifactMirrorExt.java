package acme;

import java.nio.charset.Charset;
import java.util.concurrent.Semaphore;

import cartago.*;
import io.netty.handler.codec.mqtt.MqttQoS;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClient;
import io.vertx.core.json.JsonObject;
import io.vertx.mqtt.MqttClient;
import io.vertx.mqtt.MqttClientOptions;

public class SmartCMArtifactMirrorExt extends Artifact {

	private Vertx vertx;
	
	private String serviceHost;
	private int servicePort;
	private JsonObject currentState;
	
	void init(String host, int remotePort) {
		this.serviceHost = host;
		this.servicePort = remotePort;
		this.vertx = Vertx.vertx();
		
		this.defineObsProperty("coffeeLevel", 0);

		setup();
	}
	

	void setup() {
		MqttClientOptions options = new MqttClientOptions().setKeepAliveTimeSeconds(2);
		MqttClient client = MqttClient.create(Vertx.vertx(), options);
		
		SmartCMArtifactMirrorExt ref = this;
		
		// handler will be called when we have a message in topic we subscribing for
		client.publishHandler(publish -> {
			System.out.println("Just received message on [" + publish.topicName() + "] payload [" + publish.payload().toString(Charset.defaultCharset()) + "] with QoS [" + publish.qosLevel() + "]");
			JsonObject state = publish.payload().toJsonObject();
			System.out.println(state);
			ref.beginExternalSession();
			this.updateState(state);
			ref.endExternalSession(true);
		});

		// handle response on subscribe request
		client.subscribeCompletionHandler(h -> {
			System.out.println("Receive SUBACK from server with granted QoS : " + h.grantedQoSLevels());
		});

		// connect to a server
		client.connect(1883, serviceHost, ch -> {
			if (ch.succeeded()) {
				System.out.println("Connected to a server");
				client.subscribe("coffee-machine-state", 0);
			} else {
				System.out.println("Failed to connect to a server");
				System.out.println(ch.cause());
			}
		});
		
	}
	void updateState(JsonObject state) {
		// log("updating obs prop with " + cf);
		this.currentState = state;
		int coffeeLevel = state.getInteger("coffee-level");
		ObsProperty prop = this.getObsProperty("coffeeLevel");
		if (prop.intValue() != coffeeLevel) {
			prop.updateValue(coffeeLevel);
		}
	}
	

}
