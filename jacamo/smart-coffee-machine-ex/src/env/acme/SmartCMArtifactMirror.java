package acme;

import java.util.concurrent.Semaphore;

import cartago.*;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;

public class SmartCMArtifactMirror extends Artifact {

	private Vertx vertx;
	
	private String serviceHost;
	private int servicePort;
	
	void init(String host, int remotePort) {
		this.serviceHost = host;
		this.servicePort = remotePort;
		this.vertx = Vertx.vertx();
		
		this.defineObsProperty("coffeeLevel", 0);

		startUpdatingProc();
	}
	
	
	void startUpdatingProc() {
		SmartCMArtifactMirror ref = this;
		vertx.setPeriodic(1000, (res) -> {
			HttpClient client = vertx.createHttpClient();
			client.get(servicePort, serviceHost, "/api/properties/coffee-level", response -> {
				// log("Received response with status code " + response.statusCode());
				response.bodyHandler(body -> {
					int cf = body.toJsonObject().getInteger("coffeeLevel");
					// System.out.println("[VERTX Handler] coffee level: " + cf);
					ref.beginExternalSession();
					ref.updateCoffeeLevel(cf);
					ref.endExternalSession(true);						
				});
			}).end();
		});
		
	}
	
	void updateCoffeeLevel(int cf) {
		// log("updating obs prop with " + cf);
		ObsProperty prop = this.getObsProperty("coffeeLevel");
		if (prop.intValue() != cf) {
			prop.updateValue(cf);
		}
	}
	

}
