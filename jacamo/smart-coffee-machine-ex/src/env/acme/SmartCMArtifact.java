package acme;

import java.util.concurrent.Semaphore;

import cartago.*;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;

public class SmartCMArtifact extends Artifact {

	private Vertx vertx;
	
	private String serviceHost;
	private int servicePort;
	
	void init(String host, int remotePort) {
		this.serviceHost = host;
		this.servicePort = remotePort;
		this.vertx = Vertx.vertx();
		
	}
	
	/* async action, with event signaled */
	
	@OPERATION void requestCoffeeLevel() {
		HttpClient client = vertx.createHttpClient();
		SmartCMArtifact ref = this;
		client.get(servicePort, serviceHost, "/api/properties/coffee-level", response -> {
			log("Received response with status code " + response.statusCode());
			response.bodyHandler(body -> {
				int cf = body.toJsonObject().getInteger("coffeeLevel");
				System.out.println("[VERTX Handler] coffee level: " + cf);
				ref.beginExternalSession();
				ref.notifyCoffeeLevel(cf);
				ref.endExternalSession(true);	
			});
		}).end();
	}
	
	void notifyCoffeeLevel(int cf) {
		log("signaling: " + cf);
		signal("newCoffeeLevel",cf);
	}

	/* synchronous action */
	
	@OPERATION void getCoffeeLevel(OpFeedbackParam<Integer> coffeeLevel) {
		HttpClient client = vertx.createHttpClient();
		CoffeeLevelResult res = new CoffeeLevelResult();
		client.get(servicePort, serviceHost, "/api/properties/coffee-level", response -> {
			log("Received response with status code " + response.statusCode());
			response.bodyHandler(body -> {
				int cf = body.toJsonObject().getInteger("coffeeLevel");
				System.out.println("[VERTX Handler] coffee level: " + cf);
				res.notifyResult(cf);
			});
		}).end();

		try {
			int cf = res.waitForResult();
			coffeeLevel.set(cf);
		} catch (Exception ex) {
			ex.printStackTrace();
		}	
	}
		
	class CoffeeLevelResult {
		
		private boolean resultAvail;
		private int coffeeLevel;
		
		public CoffeeLevelResult() {
			resultAvail = false;
		}
		
		public synchronized int waitForResult() throws Exception {
			while (!resultAvail) {
				wait();
			}
			return coffeeLevel;
		}
		
		public synchronized void notifyResult(int coffeeLevel) {
			this.coffeeLevel = coffeeLevel;
			this.resultAvail = true;
			this.notifyAll();
		}
	}
}
