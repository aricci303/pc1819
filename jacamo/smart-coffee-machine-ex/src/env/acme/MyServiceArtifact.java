package acme;

import cartago.*;
import io.vertx.core.Vertx;

public class MyServiceArtifact extends Artifact {

	private Vertx vertx;
	
	void init() {		
		this.vertx = Vertx.vertx();
	}
	
	@OPERATION void start() {
		MyService service = new MyService();
		vertx.deployVerticle(service);
	}
}
