!test0.

+!test0 
	<- 	makeArtifact("service","acme.SmartCMArtifact",["localhost",10000],Id);
	    focus(Id);
		requestCoffeeLevel.
		
+newCoffeeLevel(L)
	<- println("coffee level: ",L).

+!test1
	<- 	makeArtifact("service","acme.SmartCMArtifact",["localhost",10000],Id);
		getCoffeeLevel(L);
		println(L).


+!test2
	<- 	makeArtifact("service","acme.SmartCMArtifactMirror",["localhost",10000],Id);
		focus(Id).
		
+coffeeLevel(L)
	<- println("coffee level: ", L).
	
	
+!test3
	<- 	makeArtifact("service","acme.SmartCMArtifactMirrorExt",["localhost",10000],Id);
		focus(Id).
	