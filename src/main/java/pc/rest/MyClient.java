package pc.rest;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.WebSocket;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class MyClient extends AbstractVerticle {

	private static int servicePort = 10000;
	private String host;
	private int port;
	
	public MyClient(String host, int port) {
		this.host = host;
		this.port = port;
	}
	
	public void start(){
		
		HttpClient client = this.getVertx().createHttpClient();
				
		client.get(port, host, "/api/resources", response -> {
			System.out.println("Received response with status code " + response.statusCode());
			response.bodyHandler(bodyHandler -> {
				JsonArray list = bodyHandler.toJsonArray();
				list.stream().forEach(obj -> {
					System.out.print(obj.toString()+"\n");
				});
			});
		}).end();

		JsonObject item = new JsonObject().put("id", "res"+System.currentTimeMillis()).put("prop", System.currentTimeMillis());
		client.post(port, host, "/api/resources", response -> {
			System.out.println("Received response with status code " + response.statusCode());
			response.bodyHandler(bodyHandler -> {
				System.out.println(bodyHandler.toString());
			});
		}).putHeader("content-type", "application/json").end(item.encodePrettily());
		
		client.websocket(port, host, "/api/resources", this::webSocketHandler);
		
	}

	private void webSocketHandler(WebSocket ws) {
		ws.handler(data -> {
			String s = data.toString().substring(4);
			JsonObject obj = new JsonObject(s);
			int count = obj.getInteger("count");
			System.out.println("Received data: " + count);
			try {
				Thread.sleep(500);
				ping(ws,count);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		});		
		ping(ws,0);		
	}
	
	private void ping(WebSocket ws, int count) {
		JsonObject obj = new JsonObject().put("count",count);
		Buffer buffer = Buffer.buffer();
		obj.writeToBuffer(buffer);		
		ws.write(buffer);		
	}
	
	public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();
		vertx.deployVerticle(new MyClient("localhost",servicePort));
	}
}
