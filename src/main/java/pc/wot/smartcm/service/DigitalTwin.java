package pc.wot.smartcm.service;

public class DigitalTwin {

	public enum ConnectionState { Connected, NotConnected }
	private ConnectionState connectionState;
		
	public DigitalTwin() {
		connectionState = ConnectionState.NotConnected;
	}
	
	public void setConnectionState(ConnectionState state) {
		this.connectionState = state;
	}

	public ConnectionState getConnectionState() {
		return this.connectionState;
	}
	
	public boolean isConnected() {
		return this.connectionState.equals(ConnectionState.Connected);
	}
}
