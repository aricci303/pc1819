package pc.wot.smartcm.service;

import io.netty.handler.codec.mqtt.MqttQoS;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.mqtt.MqttServer;
import io.vertx.mqtt.MqttTopicSubscription;
import pc.wot.smartcm.service.handlers.ChangeState;
import pc.wot.smartcm.service.handlers.GetCoffeeLevel;
import pc.wot.smartcm.service.handlers.GetCoffeeLevelThreshold;
import pc.wot.smartcm.service.handlers.GetNumCoffeeDelivered;
import pc.wot.smartcm.service.handlers.SetCoffeeLevelThreshold;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SmartCMService extends AbstractVerticle {

	private static final int NOT_AVAILABLE = 501;
	
	private int localPort;

	private SmartCMModel machine;
	
	public SmartCMService(int port) {
		this.localPort = port;	
	}

	@Override
	public void start() {
		
		/* prepare the digital twin */		
		machine = SmartCMModel.connect();

		setup();
		
		initMQTTService();

		log("Ready");
	}

	private void setup() {
		Router router = Router.router(vertx);
		router.route().handler(BodyHandler.create());
		
		/* obs prop */
		router.get("/api/properties/coffee-level").handler(new GetCoffeeLevel(this));
		
		/* obs prop with sub */
		router.get("/api/properties/num-coffee-delivered").handler(new GetNumCoffeeDelivered(this));
		router.post("/api/properties/num-coffee-delivered/subs").handler(this::handleNotAvailableYet);
		
		/* threshold */
		router.get("/api/properties/coffee-level-threshold").handler(new GetCoffeeLevelThreshold(this));
		router.put("/api/properties/coffee-level-threshold").handler(new SetCoffeeLevelThreshold(this));

		/* actions */
		router.post("/api/actions/change-state").handler(new ChangeState(this));

		/* events */
		
		router.post("/api/events/problem-detected/subs").handler(this::handleNotAvailableYet);
		router.post("/api/events/low-coffee/subs").handler(this::handleNotAvailableYet);

		vertx
			.createHttpServer()
			.requestHandler(router::accept)
			.listen(localPort);
		
	}
	
	public SmartCMModel getMachine(){
		return machine;
	}
	
	private void handleNotAvailableYet(RoutingContext routingContext) {
				sendError(routingContext,NOT_AVAILABLE);
	}

	public void exec(Handler<Future<Object>> code, Handler res) {
		this.getVertx().executeBlocking(code, res);
	}
	
	private void sendError(RoutingContext routingContext, int code) {
		routingContext.response().setStatusCode(code).end();
	}

	public void log(String msg) {
		System.out.println("[SERVICE] "+msg);
	}
	
	public void post(String url, JsonObject msg, Handler<AsyncResult> hnd) {
		
		String host = url.substring(0, url.indexOf(":"));
		String rem = url.substring(url.indexOf(":") + 1);
		int port = Integer.parseInt(rem.substring(0, rem.indexOf("/")));
		String uri = rem.substring(rem.indexOf("/"));

		WebClient client = WebClient.create(this.getVertx());
		Buffer buf = Buffer.buffer();
		msg.writeToBuffer(buf);
		client
		  .post(port, host, uri)
		  .sendBuffer(buf, (Handler) hnd);
	}
	
	
	public void initMQTTService() {
		MqttServer mqttServer = MqttServer.create(vertx);

		mqttServer
		.endpointHandler(endpoint -> {
			// shows main connect info
			System.out.println("MQTT client [" + endpoint.clientIdentifier() + "] request to connect, clean session = " + endpoint.isCleanSession());
			System.out.println("[keep alive timeout = " + endpoint.keepAliveTimeSeconds() + "]");

			// accept connection from the remote client
			endpoint.accept(false);

			// handling requests for subscriptions
			endpoint.subscribeHandler(subscribe -> {

				List<MqttQoS> grantedQosLevels = new ArrayList<>();
				for (MqttTopicSubscription s : subscribe.topicSubscriptions()) {
					grantedQosLevels.add(s.qualityOfService());
				}
				// ack the subscriptions request
				endpoint.subscribeAcknowledge(subscribe.messageId(), grantedQosLevels);

				/* simulating updates of the coffee level...*/
				
				int cl = machine.getCoffeeLevel();
						
				endpoint.publish("coffee-machine-state",
						Buffer.buffer("{ \"coffee-level\": " + cl + " }"),
						MqttQoS.AT_LEAST_ONCE,
						false,
						false);

				// specifing handlers for handling QoS 1 and 2
				endpoint.publishAcknowledgeHandler(messageId -> {

					System.out.println("Received ack for message = " + messageId);

				}).publishReceivedHandler(messageId -> {

					endpoint.publishRelease(messageId);

				}).publishCompletionHandler(messageId -> {

					System.out.println("Received ack for message = " + messageId);
				});
			});

			// handling incoming published messages
			endpoint.publishHandler(message -> {

				System.out.println("Just received message on [" + message.topicName() + "] payload [" + message.payload() + "] with QoS [" + message.qosLevel() + "]");

				if (message.qosLevel() == MqttQoS.AT_LEAST_ONCE) {
					endpoint.publishAcknowledge(message.messageId());
				} else if (message.qosLevel() == MqttQoS.EXACTLY_ONCE) {
					endpoint.publishReceived(message.messageId());
				}

			}).publishReleaseHandler(messageId -> {
				endpoint.publishComplete(messageId);
			});
		})
		.listen(1883, "0.0.0.0", ar -> {

			if (ar.succeeded()) {
				System.out.println("Smart Coffee Machine MQTT server is listening on port " + mqttServer.actualPort());
			} else {
				System.err.println("Error on starting the server" + ar.cause().getMessage());
			}
		});
	}
}