package pc.wot.smartcm.service;

import io.vertx.core.Vertx;

public class ServiceMain  {

	public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();
		SmartCMService service = new SmartCMService(10000);
		vertx.deployVerticle(service);
	}

}