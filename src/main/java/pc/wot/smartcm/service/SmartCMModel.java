package pc.wot.smartcm.service;

public class SmartCMModel extends DigitalTwin {

	public enum WorkingMode { Booting, Normal, Maintenance, Off}
	private int coffeeLevel;
	private int numCoffeeDelivered;
	private int sugarLevel;
	private WorkingMode currentMode;
	private int coffeeLevelThreshold;
	
	private SmartCMModel() {
		this.coffeeLevel = 100;
		this.sugarLevel = 0;
		currentMode = WorkingMode.Booting; 
	}
	
	public void updateCoffeeLevel(int delta) {
		this.coffeeLevel += delta;
	}

	public int getCoffeeLevel() {
		return this.coffeeLevel;
	}

	public int getSugarLevel() {
		return this.sugarLevel;
	}
	
	public int getCoffeeLevelThreshold() {
		return coffeeLevelThreshold;
	}

	public void setCoffeeLevelThreshold(int level) {
		coffeeLevelThreshold = level;
	}

	public void updateLevel(int delta) {
		this.coffeeLevel += delta;
	}

	public int getNumCoffeeDelivered() {
		return numCoffeeDelivered;
	}
	
	public void changeModality(WorkingMode mode) {
		this.currentMode = mode;
	}

	public WorkingMode getCurrentWorkingModality() {
		return this.currentMode;
	}
	
	static public SmartCMModel connect() {
		SmartCMModel dt = new SmartCMModel();
		dt.setConnectionState(ConnectionState.Connected);
		return dt;
	}
}
