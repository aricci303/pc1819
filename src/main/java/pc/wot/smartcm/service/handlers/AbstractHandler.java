package pc.wot.smartcm.service.handlers;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import pc.wot.smartcm.service.SmartCMService;

public abstract class AbstractHandler implements Handler<RoutingContext>{

	private SmartCMService service;
	
	protected AbstractHandler(SmartCMService service) {
		this.service = service;
	}
	
	protected SmartCMService getService() {
		return service;
	}
	
	protected void sendReply(RoutingContext ctx, JsonObject obj) {
		ctx.response().putHeader("content-type", "application/json").end(obj.encodePrettily());
	}
	
	protected void sendNotAvailable(RoutingContext routingContext) {
		routingContext.response().setStatusCode(501).end();
	}

	protected void sendAccepted(RoutingContext routingContext) {
		routingContext.response().setStatusCode(202).end();
	}
	
	protected void sendCreated(RoutingContext routingContext) {
		routingContext.response().setStatusCode(201).end();
	}
	
	protected void post(String url, JsonObject msg, Handler<AsyncResult> hnd)  {
		service.post(url, msg, hnd);
	}
	
	protected void exec(Handler<Future<Object>> code, Handler res) {
		service.exec(code,res);
	}
	
	protected void log(String st) {
		service.log(st);
	}
	
}
