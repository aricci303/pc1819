package pc.wot.smartcm.service.handlers;

import io.vertx.core.Handler;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import pc.wot.smartcm.service.SmartCMModel;
import pc.wot.smartcm.service.SmartCMService;

public class GetCoffeeLevel extends AbstractHandler {

	public GetCoffeeLevel(SmartCMService service) {
		super(service);
	}	

	@Override
	public void handle(RoutingContext ctx) {
		HttpServerResponse response = ctx.response();
		SmartCMModel machine = getService().getMachine();
		if (machine.isConnected()) {
			JsonObject obj = new JsonObject();
			obj.put("coffeeLevel", getService().getMachine().getCoffeeLevel());
			response.putHeader("content-type", "application/json").end(obj.encodePrettily());
		} else {
			sendNotAvailable(ctx);
		}
	}

}
