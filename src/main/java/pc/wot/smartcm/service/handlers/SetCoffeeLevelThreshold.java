package pc.wot.smartcm.service.handlers;

import io.vertx.core.Handler;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import pc.wot.smartcm.service.SmartCMModel;
import pc.wot.smartcm.service.SmartCMService;

public class SetCoffeeLevelThreshold extends AbstractHandler {

	public SetCoffeeLevelThreshold(SmartCMService service) {
		super(service);
	}	

	@Override
	public void handle(RoutingContext ctx) {
		HttpServerResponse response = ctx.response();
			SmartCMModel machine = getService().getMachine();
			if (machine.isConnected()) {
				JsonObject msg = ctx.getBodyAsJson();
				int level = msg.getInteger("coffeeLevelThreshold");
				machine.setCoffeeLevelThreshold(level);
				response.end();
			} else {
				sendNotAvailable(ctx);
			}
	}

}
