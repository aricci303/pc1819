package pc.wot.smartcm.service.handlers;

import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import pc.wot.smartcm.service.SmartCMModel;
import pc.wot.smartcm.service.SmartCMService;
import pc.wot.smartcm.service.SmartCMModel.WorkingMode;

public class ChangeState extends AbstractHandler {

	public ChangeState(SmartCMService service) {
		super(service);
	}	

	@Override
	public void handle(RoutingContext ctx) {
		HttpServerRequest request = ctx.request();
		// request.bodyHandler(buffer -> {
			JsonObject msg = ctx.getBodyAsJson();
			SmartCMModel machine = getService().getMachine();
			if (machine.isConnected()) {
					exec((Future<Object> fut) -> {
						String mod = msg.getString("modality");
						if (mod.equals("normal")) {
							machine.changeModality(WorkingMode.Normal);
						} else if (mod.equals("maintenance")) {
							machine.changeModality(WorkingMode.Maintenance);
						}
						fut.complete();
					}, res -> {
						String method = msg.getString("method");
						if (method.equals("webhook")) {
							String url = msg.getString("callback");
							JsonObject obj = new JsonObject();
							obj.put("result", "ok");
							post(url, obj, ar -> {
								if (ar.succeeded()) {
									log("succeded");
								} else {
									log("failed");
								}
							});
						} else if (method.equals("websocket")) {
							JsonObject obj = new JsonObject();
							obj.put("result", "ok");
							ServerWebSocket sock = ctx.request().upgrade();
						    sock.write(obj.toBuffer());
						    sock.close();
						    log("succeeded");
						}							
					});
					sendAccepted(ctx);
			} else {
				sendNotAvailable(ctx);
			}		
		// });
	}

}
