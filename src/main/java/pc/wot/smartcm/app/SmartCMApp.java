package pc.wot.smartcm.app;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.handler.BodyHandler;
import pc.wot.smartcm.app.handlers.ChangeStateResult;
import pc.wot.smartcm.service.handlers.GetCoffeeLevel;
import pc.wot.smartcm.service.handlers.GetCoffeeLevelThreshold;
import pc.wot.smartcm.service.handlers.GetNumCoffeeDelivered;
import pc.wot.smartcm.service.handlers.SetCoffeeLevelThreshold;

import java.util.HashMap;
import java.util.Map;

public class SmartCMApp extends AbstractVerticle {

	private static final int NOT_AVAILABLE = 501;
	
	private int localPort;
	private String serviceHost;
	private int servicePort;
	
	public SmartCMApp(int localPort, String host, int remotePort) {
		this.localPort = localPort;	
		this.serviceHost = host;
		this.servicePort = remotePort;
		
	}

	@Override
	public void start() {
		setupServer();
		
		HttpClient client = this.getVertx().createHttpClient();
		client.get(servicePort, serviceHost, "/api/properties/coffee-level", response -> {
			System.out.println("Received response with status code " + response.statusCode());
			response.bodyHandler(body -> {
				int cf = body.toJsonObject().getInteger("coffeeLevel");
				log("coffee level: "+ cf);
			});
		}).end();

		
		JsonObject msg = new JsonObject();
		msg.put("coffeeLevelThreshold",20);
		client.put(servicePort, serviceHost, "/api/properties/coffee-level-threshold", response -> {
			System.out.println("Received response with status code " + response.statusCode());
			/*
			response.bodyHandler(body -> {
				int cf = body.toJsonObject().getInteger("coffeeLevel");
				log("coffee level: "+ cf);
			});*/
		}).end(msg.encodePrettily());
		 
		
		msg = new JsonObject();
		msg.put("modality","maintenance");
		msg.put("method","webhook");
		msg.put("callback","localhost:"+localPort+"/api/results");
		client.post(servicePort, serviceHost, "/api/actions/change-state", response -> {
			System.out.println("Received response with status code " + response.statusCode());
			
		}).end(msg.encodePrettily());
		
		

	}

	private void setupServer() {
		Router router = Router.router(vertx);
		router.route().handler(BodyHandler.create());
		
		router.post("/api/results").handler(new ChangeStateResult(this));
			
		// setup server-side
		
		vertx
			.createHttpServer()
			.requestHandler(router::accept)
			.listen(localPort);
		
	}
	
	private void handleNotAvailableYet(RoutingContext routingContext) {
				sendError(routingContext,NOT_AVAILABLE);
	}

	public void exec(Handler<Future<Object>> code, Handler res) {
		this.getVertx().executeBlocking(code, res);
	}
	
	private void sendError(RoutingContext routingContext, int code) {
		routingContext.response().setStatusCode(code).end();
	}

	public void log(String msg) {
		System.out.println("[APP] "+msg);
	}
	
	public void post(String url, JsonObject msg, Handler<AsyncResult> hnd) {
		
		String host = url.substring(0, url.indexOf(":"));
		String rem = url.substring(url.indexOf(":") + 1);
		int port = Integer.parseInt(rem.substring(0, rem.indexOf("/")));
		String uri = rem.substring(rem.indexOf("/"));

		WebClient client = WebClient.create(this.getVertx());
		Buffer buf = Buffer.buffer();
		msg.writeToBuffer(buf);
		client
		  .post(port, host, uri)
		  .sendBuffer(buf, (Handler) hnd);
	}
}