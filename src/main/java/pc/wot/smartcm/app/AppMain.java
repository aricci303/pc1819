package pc.wot.smartcm.app;

import io.vertx.core.Vertx;

public class AppMain  {

	public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();
		SmartCMApp service = new SmartCMApp(10001,"localhost",10000);
		vertx.deployVerticle(service);
	}

}