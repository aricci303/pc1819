package pc.wot.smartcm.app.handlers;

import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import pc.wot.smartcm.app.SmartCMApp;
import pc.wot.smartcm.service.SmartCMModel;
import pc.wot.smartcm.service.SmartCMService;
import pc.wot.smartcm.service.SmartCMModel.WorkingMode;

public class ChangeStateResult extends AbstractHandler {

	public ChangeStateResult(SmartCMApp service) {
		super(service);
	}	

	@Override
	public void handle(RoutingContext ctx) {
		log("here.");
		ctx.response().end();
	}

}
